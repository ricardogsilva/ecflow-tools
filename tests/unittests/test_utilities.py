# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for ecflowtools.utilities"""

import mock
import pytest

from ecflowtools import utilities

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("path, expected_handler", [
    (
        "mypackage.mymodule.some_func",
        "_import_python_object_as_python_path"
    ),
    (
        "~/dev/project/mypackage/mymodule.py:some_func",
        "_import_python_object_from_filesystem_path"
    ),
])
def test_import_python_object(path, expected_handler):
    with mock.patch("ecflowtools.utilities.{}".format(expected_handler),
                    autospec=True) as mock_handler:
        utilities.import_python_object(path)
        mock_handler.assert_called_once_with(path)
