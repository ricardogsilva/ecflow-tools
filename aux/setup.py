from setuptools import setup, find_packages, Distribution


class BinaryDistribution(Distribution):

    def has_ext_modules(foo):
        return True


setup(
    name="ecflow",
    version="4.6.1",
    description="Python bindings for ECMWF's ecFlow",
    packages=find_packages(where="src"),
    package_dir={"":"src"},
    include_package_data=True,
    distclass=BinaryDistribution
)
