# Copyright 2017 Ricardo Garcia Silva
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import io
from glob import glob
from os.path import basename, dirname, join, splitext

from setuptools import find_packages, setup


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf-8")
    ).read()


setup(
    name="ecflowtools",
    version=read("VERSION"),
    description="A framework for organizing your code",
    long_description=read("README.rst"),
    author="Ricardo Garcia Silva",
    author_email="ricardo.garcia.silva@likeno.pt",
    url="http://gitlab.com/ricardogsilva/ecflow-tools",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 2",
    ],
    platforms=[""],
    license="Apache license",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    py_modules=[
        splitext(basename(path))[0] for path in glob("src/*.py")],
    include_package_data=True,
    install_requires=[
        #"ecflow",
        "pathlib2",
        "python-dateutil",
        "PyYAML",
    ],
    entry_points={
        "console_scripts": [
            "ecflow_definition_generator = ecflowtools.suitegenerator:main"
        ]
    }
)
