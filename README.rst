ecflow-tools
============

This project has some tools that are useful for working with
`ECMWF's ecflow`_. They are tailored specifically for usage by
the Earth Observation projects team at IPMA. However, the goal is for these
tools to be generic enough in order to be used in other contexts as well.


Installation
------------

In order to work with this code you need to have the ecflow python bindings
installed. After that, just pip install this project by providing it the
repository's URL

Quickstart
----------

ecflow-tools' main functionality is the automatic generation of ecflow suites
given some previously prepared configuration file. Generate the following
suite configuration file::

    # my-ecflow-config.yml
    ---
    sample_suite:
      ecf_variables:
        SCRIPTS_DIR: "~/my-ecflow-tools-scripts"
        OPERATIONAL_STATUS: test
      families:
        temporal_frequency:
          strategy: hourly
          hours:
            - 0
            - 1
            - 2
        first:
          tasks:
            my_first_task:
              temporal_trigger_offset: 1
              python_callback: "%SCRIPTS_DIR%/module1.py:main"
            the_second_task:
              dependency_trigger: my_first_task eq complete
              python_callback: "%SCRIPTS_DIR%/module1.py:some_function"

Generate the following file as well::

    # ~/my-ecflow-tools-scripts/module1.py
    def main(**kwargs):
        print("Hi")

    def some_function(**kwargs):
        print("Simulating some processing...")


We start by creating a sample configuration file `my-ecflow-config.yml`. This
is a `YAML`_ file that describes the configuration of the ecflow suites that
we'd like to generate.
We also create a regular python module, with two functions. These functions
are referenced in the configuration file.They are going to be bound to each
task...


.. _ECMWF's ecflow: https://software.ecmwf.int/wiki/display/ECFLOW/ecflow+home
.. _YAML: http://yaml.org/
