# A Dockerfile for building an ecflow image
#
# This image has the server, client, python2 bindings and ecflow-ui
#

FROM ubuntu:16.04

ENV \
  ECFLOW_VERSION=4.6.1 \
  BOOST_VERSION=1_53_0

WORKDIR /tmp

COPY . ./ecflow-tools

RUN apt-get update \
  && apt-get install --yes \
    build-essential \
    cmake \
    python-dev \
    python-pip \
    qt5-default \
    wget \
  && wget \
    --output-document=ecflow-${ECFLOW_VERSION}.tar.gz \
    https://software.ecmwf.int/wiki/download/attachments/8650755/ecFlow-${ECFLOW_VERSION}-Source.tar.gz?api=v2 \
  && tar -xzf ecflow-${ECFLOW_VERSION}.tar.gz \
  && wget \
    --output-document=boost_${BOOST_VERSION}.tar.gz \
    https://software.ecmwf.int/wiki/download/attachments/8650755/boost_${BOOST_VERSION}.tar.gz?api=v2 \
  && tar -xzf boost_${BOOST_VERSION}.tar.gz \
  && export WK=$(pwd)/ecFlow-${ECFLOW_VERSION}-Source \
  && export BOOST_ROOT=$(pwd)/boost_${BOOST_VERSION} \
  # building bjam and boost first
  && cd ${BOOST_ROOT} \
  && ./bootstrap.sh \
  && $WK/build_scripts/boost_1_53_fix.sh \
  && $WK/build_scripts/boost_build.sh \
  # now we build ecflow
  && mkdir $WK/build \
  && cd $WK/build \
  && cmake .. \
    -DENABLE_UI=ON \
    -DENABLE_GUI=OFF \
  && CPUS=$(lscpu -p | grep -v '#' | wc -l) \
  && make -j${CPUS} \
  && make install \
  # install dumb-init
  && wget \
    --output-document /usr/local/bin/dumb-init \
    https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 \
  && chmod +x /usr/local/bin/dumb-init \
  # install ecflow-tools
  && cd /tmp/ecflow-tools \
  && pip install --upgrade pip setuptools \
  && pip install --editable . \
  # cleanup
  && apt-get remove --yes \
    build-essential \
    cmake \
  && rm -rf $BOOST_ROOT \
  && rm -rf $WK \
  && rm -rf /tmp/boost_${BOOST_VERSION}.tar.gz \
  && rm -rf /tmp/ecflow-${ECFLOW_VERSION}.tar.gz

RUN useradd --create-home --shell /bin/bash ecflow

USER ecflow

RUN mkdir -p /home/ecflow/SERVER_HOME \
  && mkdir -p /home/ecflow/SUITES_HOME


#  # now we package up the ecflow python bindings as a wheel
#  && cd /tmp \
#  && mkdir -p ecflow-python/src/ecflow \
#  && mkdir -p ecflow-python/bin \
#  && cd ecflow-python \
#  && cp ${PYTHON_INSTALL_PREFIX}/ecflow.so bin \
#  && cp ${PYTHON_INSTALL_PREFIX}/__init__.py src/ecflow \
#  && cp ${PYTHON_INSTALL_PREFIX}/ecf.py src/ecflow \
#  && cp ${PYTHON_INSTALL_PREFIX}/sms2ecf.py src/ecflow \
#  && python setup.py bdist_wheel

EXPOSE 3141

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["ecflow_start.sh" "-d" "/home/ecflow/SERVER_HOME", "-p", "3141"]
