# Copyright 2017 Ricardo Garcia Silva
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Utility functions for working with ecflow"""

from contextlib import contextmanager
from functools import partial
import importlib
import logging
import signal
import sys

import ecflow
import pathlib2

logger = logging.getLogger(__name__)


@contextmanager
def task_manager(host, port, task_path, ecflow_password,
                 try_number, ecflow_rid):
    """A context manager for running ecflow tasks"""
    ecf_client = ecflow.Client(host, port)
    ecf_client.set_child_pid(ecflow_rid)
    ecf_client.set_child_path(task_path)
    ecf_client.set_child_password(ecflow_password)
    ecf_client.set_child_try_no(try_number)
    ecf_client.set_child_timeout(20)  # seconds
    signal_handler = partial(_signal_handler, ecf_client)
    signals = [
        signal.SIGINT,
        signal.SIGHUP,
        signal.SIGQUIT,
        signal.SIGILL,
        signal.SIGTRAP,
        signal.SIGIOT,
        signal.SIGBUS,
        signal.SIGFPE,
        signal.SIGUSR1,
        signal.SIGUSR2,
        signal.SIGPIPE,
        signal.SIGTERM,
        signal.SIGXCPU,
        signal.SIGPWR,
    ]
    for s in signals:
        signal.signal(s, signal_handler)
    ecf_client.child_init()
    try:
        yield ecf_client
    except Exception:
        msg = "task aborted"
        logger.exception(msg)
    finally:
        ecf_client.child_complete()


def _signal_handler(ecf_client, signal_number, stack):
    ecf_client.child_abort()
    raise RuntimeError("Received signal {!r}".format(signal_number))


def get_ecflow_variables(node_path, ecflow_defs):
    node = ecflow_defs.find_abs_node(node_path)
    node_variables = {v.name(): v.value() for v in node.variables}
    parent = node.get_parent()
    while parent is not None:
        node = parent
        node_variables.update(
            {v.name(): v.value() for v in node.variables})
        parent = node.get_parent()
    return node_variables


def import_python_object(path):
    if ":" in path:
        handler = _import_python_object_from_filesystem_path
    else:
        handler = _import_python_object_as_python_path
    return handler(path)


def _import_python_object_from_filesystem_path(path):
    filesystem_path, python_object_name = path.rpartition(":")[::2]
    full_path = pathlib2.Path(filesystem_path)
    sys.path.append(full_path.parent)
    module_object = importlib.import_module(full_path.stem)
    return getattr(module_object, python_object_name)


def _import_python_object_as_python_path(path):
    module_path, python_object_name = path.rpartition(".")[::2]
    module_object = importlib.import_module(module_path)
    return getattr(module_object, python_object_name)
