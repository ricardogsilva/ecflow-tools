# Copyright 2017 Ricardo Garcia Silva
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A generic ecflow task.

The actual processing code can be found on the paths pointed to by the
``PYTHON_CALLBACK`` variable that exists for every task in the suite.

When ran through ecflow, this script is called by the ecflow server using its
ECF_JOB_CMD variable. ECF_JOB_CMD is expected to make use of a python
interpreter and to call this script for execution in standalone mode. An
example ECF_JOB_CMD is

    /usr/bin/python %ECF_JOB% 1> %ECF_JOBOUT% 2>&1

Ecflow does not call this script directly though. First it makes a copy of
the file (generating the job file) and it performs a pre-processing step in
which it replaces all special strings with the relevant variables. A special
string is a string that is surrounded by the edit character, which is
usually the percent character. Consult the ecflow documentation for more
information on how ecflow variables can be used to add dynamic information
to ecflow tasks.

after generating the job file, ecflow uses its ECF_JOB_CMD variable to figure
out how to run the job. In this case, it will call some python interpreter and
execute the job file.

"""

import logging
import os

import ecflow

from ecflowtools import utilities

logger = logging.getLogger(__name__)


def main(host, port, task_path, ecflow_password, try_number,
         python_callback, communicate_with_server=True):
    """Main entry point for ecflow to request that some code be executed.

    Parameters
    ----------
    host: str
        Host where the ecflow server is running.
    port: int
        Port where the ecflow server is running on the host
    task_path: str
        Full ecflow path to the currently executing task
    ecflow_password: str
        A string used by ecflow to aid communication between server and client
    try_number: int
        Current execution try
    python_callback: str
        Path to a custom function that holds the actual code to be executed.
        This path will be resolved to a python function and this function will
        be imported and called with all of the available ecflow variables as
        its context
    communicate_with_server: bool, optional
        Whether the communication with the ecflow server should be performed
        by this wrapper script or by the python callback function. When the
        communication is done by the current script the ecflow server is
        always notified immediately that a task is running. If on the other
        hand the communication is done on the callback side, there may be other
        (possibly asynchronous) mechanisms to go through before actually
        starting execution and the callback is free to decide when it should
        start talking with the ecflow server.

    """

    ecflow_client = ecflow.Client(host, port)
    ecflow_client.sync_local()
    defs = ecflow_client.get_defs()
    task_variables = utilities.get_ecflow_variables(
        node_path=task_path, ecflow_defs=defs)
    handler = {
        True: responsible_wrapper,
        False: deferred_wrapper
    }[communicate_with_server]
    return handler(host, port, task_path, ecflow_password, try_number,
                   python_callback, context=task_variables)


def responsible_wrapper(host, port, task_path, ecflow_password,
                        try_number, callback, context):
    """Execute the external code and communicate with the ecflow server

    this function is called *responsible_wrapper* because it takes care of
    communicating with the ecflow server, notifying it when the execution has
    begun and also when it has finished.

    """

    ecflow_rid = os.getpid()
    with utilities.task_manager(host, port, task_path, ecflow_password,
                                try_number, ecflow_rid):
        return callback(**context)


def deferred_wrapper(host, port, task_path, ecflow_password, try_number,
                     callback, context):
    """Execute the external code.

    This function is called *deferred_wrapper* because it defers communication
    with the ecflow server to the external code.

    """

    expanded_context = {
        "host": host,
        "port": port,
        "task_path": task_path,
        "ecflow_password": ecflow_password,
        "try_number": try_number,
    }
    expanded_context.update(context)
    return callback(**expanded_context)


if __name__ == "__main__":
    debug = True if "%DEBUG%".lower() in ["true", "1"] else False
    communicate_with_ecflow_server = (
        True if "%TASK_WRAPPER_COMMUNICATES_WITH_SERVER%".lower() in [
            "true", "1"] else False)
    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)
    result = main(
        host="%ECF_NODE%",
        port=int("%ECF_PORT%"),
        task_path="%ECF_NAME%",
        ecflow_password="%ECF_PASS%",
        try_number=int("%ECF_TRYNO%"),
        python_callback="%PYTHON_CALLBACK%",
        communicate_with_server=communicate_with_ecflow_server
    )
