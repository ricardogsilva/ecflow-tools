# Copyright 2017 Ricardo Garcia Silva
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Generate ecflow definition files tailored for IPMA's EO projects

"""

import argparse
import datetime as dt
from itertools import product
import logging
import os
from shutil import copy, rmtree
from tempfile import mkdtemp

import ecflow
import dateutil.parser
import pathlib2
import yaml

import ecflowtools
from ecflowtools import taskwrapper

logger = logging.getLogger(__name__)


def begin_remote_suites(client, defs):
    for suite in defs.suites:
        client.begin_suite(suite.get_abs_node_path(), force=False)


def copy_taskwrapper(destination_dir):
    """Copy ecflowtools.taskwrapper into destination_dir"""
    pyc_source = pathlib2.Path(taskwrapper.__file__)
    source = str(pyc_source.with_suffix(".py"))
    copy(source, destination_dir)
    return source


def create_filesystem_paths(defs):
    """Create all filesystem paths for the input defs"""
    for suite in defs.suites:
        target_dir = _get_node_variable(suite, "ECF_FILES")
        directories = create_suite_directories(suite)
        task_wrapper_path = copy_taskwrapper(directories["ECF_FILES"])
        symlink_tasks(task_wrapper_path, suite, target_dir)

def create_suite_directories(suite):
    """Create suite paths on the local filesystem"""
    directory_variables = {
        "ECF_HOME": "",
        "ECF_FILES": "",
    }
    for variable_name in directory_variables:
        value = _get_node_variable(suite, variable_name)
        path = pathlib2.Path(value)
        rmtree(str(path), ignore_errors=True)
        path.mkdir(parents=True, exist_ok=True)
        directory_variables[variable_name] = value
    return directory_variables


def generate_ecflow_definition(config, suites_home,
                               start_date=None, end_date=None):
    defs = ecflow.Defs()
    for suite_name, suite_config in config.items():
        logger.debug("Processing suite {!r}...".format(suite_name))
        suite_home = os.path.join(suites_home, suite_name)
        suite = generate_suite_from_config(
            suite_name, suite_config, suite_home,
            start_date=start_date, end_date=end_date
        )
        defs.add_suite(suite)
    defs.auto_add_externs(True)
    return defs


def generate_container_node(name, variables=None, families_config=None,
                            tasks_config=None, products=None,
                            temporal_frequency=None, parent=None,
                            repeat_strategy=None, start_date=None,
                            end_date=None, temporal_trigger_offset=None,
                            dependency_trigger=None, limits=None):
    node = ecflow.Suite(name) if parent is None else parent.add_family(name)
    if limits is not None:
        _add_node_limits(node, limits)
    node_variables = variables.copy() if variables is not None else {}
    node.add_variable(node_variables)
    _add_node_trigger(
        node,
        temporal_trigger_offset=temporal_trigger_offset,
        dependency_trigger=dependency_trigger
    )
    frequency = (temporal_frequency.copy() if temporal_frequency is not None
                 else {"strategy": None})
    temporal_strategy = frequency.pop("strategy")
    products = products or []

    if any(products) or temporal_strategy is not None:
        # we are not generating the node's tasks and families explicitly, just
        # passing them along to specialized functions that generate
        # sub-families
        if temporal_strategy is not None:
            _generate_temporal_families(
                node,
                temporal_strategy,
                strategy_parameters=frequency,
                products=products,
                tasks_config=tasks_config,
                families_config=families_config,
                repeat_strategy=repeat_strategy,
                start_date=start_date,
                end_date=end_date
            )
        elif any(products):
            for product_name in products:
                product_family_variables = {
                    "PRODUCT": product_name,
                }
                generate_container_node(
                    product_name,
                    variables=product_family_variables,
                    families_config=families_config,
                    tasks_config=tasks_config,
                    parent=node,
                    repeat_strategy=repeat_strategy,
                    start_date=start_date,
                    end_date=end_date,
                )

    else:
        tasks = tasks_config or {}
        for task_name, task_config in tasks.items():
            # get current hour and current minute and generate task only
            # if needed
            if _is_valid_time(task_config, node):
                generate_task(
                    node,
                    task_name,
                    python_callback=task_config["python_callback"],
                    temporal_trigger_offset=task_config.get(
                        "temporal_trigger_offset"),
                    dependency_trigger=task_config.get("dependency_trigger"),
                    in_limit=task_config.get("in_limit"),
                    use_label=task_config.get("use_label", True),
                    use_meter=task_config.get("use_meter", True),
                    extra_variables=task_config.get("extra_variables")
                )
        if families_config is not None:
            for family_name, family_config in families_config.items():
                generate_container_node(
                    name=family_name,
                    variables=family_config.get("variables"),
                    families_config=family_config.get("families"),
                    tasks_config=family_config.get("tasks"),
                    products=family_config.get("products"),
                    temporal_frequency=family_config.get("temporal_frequency"),
                    parent=node,
                    repeat_strategy=repeat_strategy,
                    start_date=start_date,
                    end_date=end_date,
                    temporal_trigger_offset=family_config.get(
                        "temporal_trigger_offset"),
                    dependency_trigger=family_config.get("dependency_trigger")
                )
        else:  # this is the leaf container, lets try to add the date repeat
            try:
                if repeat_strategy.lower() == "leaf":
                    node.add_repeat(ecflow.RepeatDate(
                        "YMD",
                        int(start_date.strftime("%Y%m%d")),
                        int(end_date.strftime("%Y%m%d"))
                    ))
            except AttributeError:
                pass  # repeat_strategy is None
    return node


def generate_suite(name, suite_home, repeat_strategy, limits=None,
                   start_date=None, end_date=None, variables=None,
                   tasks_config=None, products=None, temporal_frequency=None,
                   temporal_trigger_offset=None, dependency_trigger=None,
                   families_config=None,
                   wrapper_communicates_with_server=True):
    """Create an ecflow suite according to the input configuration parameters

    Some default ecflow variables are created by this function even if they
    are not provided, namely:

    * ECF_HOME - Where the suite expects to find all its files
    * ECF_FILES - where the ecflow server looks for ecf scripts

    Parameters
    ----------
    name: str
        Name for the suite
    suite_home: str, optional
    start_date: datetime.datetime, optional
    end_date: datetime.datetime, optional
    variables: dict, optional
    tasks: dict, optional
    products: list, optional
        A sequence of strings that specify top families to be generated.
    temporal_frequency:  dict, optional
    families: dict

    """

    suite_variables = {
        "ECF_HOME": os.path.join(suite_home, "ecf_home"),
        "ECF_FILES": os.path.join(suite_home, "ecf_files"),
        "PYTHON_COMMAND": "python",
        "ECF_JOB_CMD": "%PYTHON_COMMAND% %ECF_JOB% 1> %ECF_JOBOUT% 2>&1",
        "ECF_STATUS_CMD": "ps --pid %ECF_RID% -f 1> %ECF_JOBOUT%.stat 2>&1",
        "DEBUG": "False",
        "TASK_WRAPPER_COMMUNICATES_WITH_SERVER": str(
            wrapper_communicates_with_server),
    }
    suite_variables.update(variables if variables is not None else {})
    suite_limits = dict(limits) if limits is not None else {"default": 5}
    suite = generate_container_node(
        name=name,
        variables=suite_variables,
        families_config=families_config,
        tasks_config=tasks_config,
        products=products,
        temporal_frequency=temporal_frequency,
        repeat_strategy=repeat_strategy,
        start_date=start_date,
        end_date=end_date,
        temporal_trigger_offset=temporal_trigger_offset,
        dependency_trigger=dependency_trigger,
        limits=suite_limits
    )
    try:
        if repeat_strategy.lower() == "root":
            suite.add_repeat(ecflow.RepeatDate(
                "YMD",
                int(start_date.strftime("%Y%m%d")),
                int(end_date.strftime("%Y%m%d"))
            ))
    except AttributeError:
        pass
    return suite



def generate_suite_from_config(suite_name, config, suite_home,
                               start_date=None, end_date=None):
    """Generate a suite from the parsed configuration

    Parameters
    ----------
    suite_name:str
        Name of the suite
    config: dict
        A mapping with the suite configuration to use
    suite_home: str
        Full path to the directory to use for the suite's ECF_HOME

    This is a helper function to facilitate suite generation

    """
    comms = config.get("server_communication", "wrapper").lower()
    return generate_suite(
        name=suite_name,
        suite_home=suite_home,
        repeat_strategy=config.get("repeat_date"),
        limits=config.get("limits"),
        start_date=start_date,
        end_date=end_date,
        variables=config.get("ecf_variables"),
        tasks_config=config.get("tasks"),
        products=config.get("products"),
        temporal_frequency=config.get("temporal_frequency"),
        temporal_trigger_offset=config.get("temporal_trigger_offset"),
        dependency_trigger=config.get("dependency_trigger"),
        families_config=config.get("families"),
        wrapper_communicates_with_server=True if comms == "wrapper" else False
    )


def generate_task(parent, name, python_callback, temporal_trigger_offset=None,
                  dependency_trigger=None, in_limit=None, complete=None,
                  use_label=True, use_meter=True, **extra_variables):
    node = parent.add_task(name)
    if use_label:
        node.add_label("info", "")
    if use_meter:
        node.add_meter("progress", 0, 100)
    suite = _find_suite(node)
    default_limit = suite.limits.next()
    node.add_inlimit(
        str(in_limit) if in_limit is not None else default_limit.name())
    variables = {
        "PYTHON_CALLBACK": python_callback
    }
    variables.update(extra_variables)
    node.add_variable(variables)
    if complete is not None:
        node.add_complete(str(complete))
    _add_node_trigger(
        node,
        temporal_trigger_offset=temporal_trigger_offset,
        dependency_trigger=dependency_trigger
    )


def load_config(config_path):
    """Load the YAML configuration file that specifies suite configurations"""
    with open(os.path.expanduser(config_path)) as fh:
        config = yaml.safe_load(fh)
    return config


def main():
    """Entry point for this module, as used in the standalone console_script"""
    parser = _get_parser()
    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.WARNING)
    config_path = os.path.expanduser(args.configuration_path)
    logger.debug(
        "Loading configuration file {!r}".format(config_path))
    original_config = load_config(config_path)
    logger.debug("Constraining original configuration according to "
                "runtime filters...")
    config = modify_config(
        original_config,
        keep_suites=args.select_suite,
        keep_hours=args.select_hour,
        keep_minutes=args.select_minute,
        keep_products=args.select_product,
        disregard_temporal_triggers=args.disregard_temporal_triggers,
        start_task=args.start_at_task
    )
    logger.debug("Creating suite definition file...")
    defs = generate_ecflow_definition(
        config, args.suites_home,
        start_date=args.start_date, end_date=args.end_date
    )
    validate_defs(defs)
    if not args.dont_create_suite_files:
        create_filesystem_paths(defs)
    if args.ecflow_host is not None:
        client = ecflow.Client(args.ecflow_host, args.ecflow_port)
        send_defs_to_server(defs, client, force=args.force_suite_overwrite)
        if args.play_suites:
            begin_remote_suites(client, defs)
        else:
            suspend_remote_suites(client, defs)
            begin_remote_suites(client, defs)
    if args.output_definition_file:
        defs.save_as_defs(args.output_definition_file)
    else:
        print(defs)
    logger.debug("Done!")


def modify_config(config, keep_suites=None, keep_products=None,
                  disregard_temporal_triggers=False, keep_hours=None,
                  keep_minutes=None, start_task=None):
    conf = config.copy()
    hours = list(int(h) for h in keep_hours) if keep_hours is not None else []
    minutes = list(
        int(m) for m in keep_minutes) if keep_minutes is not None else []
    products = list(keep_products) if keep_products is not None else []
    rename_pattern = "{suite}"
    if keep_suites is not None:
        current_suites = set(conf.keys())
        suites_to_remove = list(current_suites.difference(set(keep_suites)))
        for suite in suites_to_remove:
            del conf[suite]
    if len(hours) > 0:
        _modify_hours(conf, hours)
        rename_pattern = "_".join(
            [rename_pattern, "hours"] + [str(h) for h in hours])
    if len(minutes) > 0:
        _modify_minutes(conf, minutes)
        rename_pattern = "_".join(
            [rename_pattern, "minutes"] + [str(m) for m in minutes])
    if any(products):
        _modify_products(conf, products)
        rename_pattern = "_".join(
            [rename_pattern, "products"] + [str(p) for p in products])
    if disregard_temporal_triggers:
        _remove_temporal_triggers_config(conf)
        rename_pattern = "_".join((rename_pattern, "no_temporal_triggers"))
    if start_task is not None:
        raise NotImplementedError
    return _rename_config_suites(conf, rename_pattern)


def send_defs_to_server(defs, ecflow_client, force=False):
    ecflow_client.sync_local()
    remote_defs = ecflow_client.get_defs()
    try:
        remote_suite_names = [suite.name() for suite in remote_defs.suites]
    except AttributeError:  # there are no remote defs
        remote_suite_names = []
    loaded_suites = []
    for suite in defs.suites:
        suite_name = suite.name()
        if suite_name in remote_suite_names:
            logger.warning(
                "Suite {!r} already exists on the server.".format(suite_name))
            if force:
                logger.debug(
                    "Overwriting suite {!r} with the current "
                    "definition...".format(suite_name)
                )
                _load_suite(suite, ecflow_client, force=force)
                loaded_suites.append(suite_name)
            else:
                logger.warning(
                    "Not sending suite {!r} to the server".format(suite_name))
        else:
            logger.debug("Sending suite {!r}".format(suite_name))
            _load_suite(suite, ecflow_client)
            loaded_suites.append(suite_name)
    return loaded_suites


def suspend_remote_suites(client, defs):
    paths_to_suspend = []
    for suite in defs.suites:
        paths_to_suspend.append(suite.get_abs_node_path())
    client.suspend(paths_to_suspend)


def symlink_tasks(source, node, target_dir):
    """Symlink all tasks defined in the input node to the source parameter"""
    for child in node.nodes:
        if isinstance(child, ecflow.Task):
            path = (
                pathlib2.Path(target_dir) / child.name()).with_suffix(".ecf")
            if not path.exists():
                path.symlink_to(source)
        else:  # it's a family
            symlink_tasks(source, node=child, target_dir=target_dir)


def to_string(suite):
    """Convert an in-memory suite definition to text."""
    defs = ecflow.Defs()
    defs.add_suite(suite)
    temporary_dir= mkdtemp(prefix="ecflowtools_")
    temporary_definition_file = os.path.join(
        temporary_dir, "{}.def".format(suite.name()))
    defs.save_as_defs(temporary_definition_file)
    with open(temporary_definition_file) as fh:
        string_definition = fh.read()
    rmtree(temporary_dir)
    return string_definition


def validate_defs(defs):
    errors = defs.check()
    if errors != "":
        raise RuntimeError("Invalid ecflow defs: {}".format(errors))


def _add_node_limits(node, limits):
    for limit_name, num_tasks in limits.items():
        node.add_limit(
            ecflow.Limit(limit_name, num_tasks))


def _add_node_trigger(node, temporal_trigger_offset=None,
                      dependency_trigger=None):
    """Add a trigger to a node"""
    if temporal_trigger_offset is not None:
        _add_temporal_trigger(node, temporal_offset=temporal_trigger_offset)
    elif dependency_trigger is not None:
        node.add_trigger(dependency_trigger)


def _add_temporal_trigger(node, temporal_offset):
    try:
        current_hour = int(_get_node_variable(node, "HOUR"))
    except TypeError:
        logger.warning("Could not find an appropriate 'HOUR' variable on the "
                       "node. Ignoring the 'temporal_offset' parameter...")
        current_hour = 0
        temporal_offset = 0
    offset = (current_hour + temporal_offset) % 24
    crosses_to_next_day = current_hour + temporal_offset > 23
    if crosses_to_next_day:
        node.add_time(offset, 0)
    else:
        node.add_today(offset, 0)


def _find_suite(node):
    """Find the suite that the input node belongs to."""
    if isinstance(node, ecflow.Suite):
        result = node
    else:
        parent = node.get_parent()
        result = _find_suite(parent)
    return result


def _generate_temporal_families(parent, strategy, strategy_parameters=None,
                                products=None, tasks_config=None,
                                families_config=None, repeat_strategy=None,
                                start_date=None, end_date=None):
    try:
        handler = {
            "hourly": _generate_hourly_families,
            "sub-hourly": _generate_minute_families,
        }[strategy]
    except KeyError:
        raise RuntimeError(
            "Invalid sub-family generation strategy: {!r}".format(strategy))
    else:
        params = strategy_parameters if strategy_parameters is not None else {}
        return handler(
            parent=parent,
            families_config=families_config,
            tasks_config=tasks_config,
            products=products,
            repeat_strategy=repeat_strategy,
            start_date=start_date,
            end_date=end_date,
            **params
        )


def _generate_hourly_families(parent, families_config=None, tasks_config=None,
                              products=None, repeat_strategy=None,
                              start_date=None, end_date=None,
                              hours=None, **kwargs):
    for hour in hours or range(24):
        generate_container_node(
            "{:02d}UTC".format(hour),
            variables={"HOUR": str(hour)},
            families_config=families_config,
            tasks_config=tasks_config,
            products=products,
            parent=parent,
            repeat_strategy=repeat_strategy,
            start_date=start_date,
            end_date=end_date,
        )


def _generate_minute_families(parent, families_config=None, tasks_config=None,
                              products=None, repeat_strategy=None,
                              start_date=None, end_date=None, hours=None,
                              minutes=None, flat=False, **kwargs):
    hours = hours or range(24)
    minutes = minutes or range(0, 60, 15)
    for hour, minute in product(hours, minutes):
        if flat:
            name = "{:02d}_{:02d}UTC".format(hour, minute)
            new_parent = parent
            variables = {
                "HOUR": str(hour),
                "MINUTE": str(minute),
            }
        else:
            hour_family_name = "{:02d}UTC".format(hour)
            try:
                new_parent = parent.add_family(hour_family_name)
                new_parent.add_variable("HOUR", str(hour))
            except RuntimeError:
                # the family was already created in a previous iteration
                new_parent = _find_child_node(parent, hour_family_name)
            name = "{:02d}".format(minute)
            variables = {
                "MINUTE": str(minute),
            }
        generate_container_node(
            name=name,
            variables=variables,
            families_config=families_config,
            tasks_config=tasks_config,
            products=products,
            parent=new_parent,
            repeat_strategy=repeat_strategy,
            start_date=start_date,
            end_date=end_date,
        )


def _find_child_node(parent, child_name):
    for child in parent.nodes:
        if child.name() == child_name:
            # ecflow mandates that sibling nodes cannot have the same name
            result = child
            break
    else:
        result = None
    return result


def _get_node_variable(node, name):
    """Utility function for retrieving a node's variable.

    This function will scan the node for a variable named after the name
    parameter and in case it cannot find it it will go up in the tree,
    searching the node's parents

    """

    for variable in node.variables:
        if variable.name() == name:
            result = variable.value()
            break
    else:
        parent = node.get_parent()
        if parent is not None:
            result = _get_node_variable(parent, name)
        else:
            result = None
    return result


def _is_valid_time(task_config, parent_node):
    except_hours = task_config.get("except_hours", [])
    if len(except_hours) > 0:
        current_hour = int(_get_node_variable(parent_node, "HOUR"))
        valid_hour = current_hour not in except_hours
    else:
        valid_hour = True
    except_minutes = task_config.get("except_minutes", [])
    if len(except_minutes) > 0:
        current_minute = int(_get_node_variable(parent_node, "MINUTE"))
        valid_minute = current_minute not in except_minutes
    else:
        valid_minute = True
    return True if valid_hour and valid_minute else False


def _load_suite(suite, ecflow_client, force=False):
    suite_defs = ecflow.Defs()
    suite_defs.add_suite(suite)
    suite_defs.auto_add_externs(True)
    return ecflow_client.load(suite_defs, force)


def _get_parser():
    now = dt.datetime.utcnow()
    parser = argparse.ArgumentParser(version=ecflowtools.__version__)
    parser.add_argument(
        "configuration_path",
        help="Path to the YAML file that has the ecflow configuration "
             "to use."
    ),
    parser.add_argument(
        "-s", "--start-date",
        type=_to_datetime,
        default=now,
        help="Start date for the suite. Accepted temporal formats are those "
             "supported by the python-dateutil package (YYYYMMDD and "
             "YYYY-MM-DD work, others might too)"
    )
    parser.add_argument(
        "-e", "--end-date",
        type=_to_datetime,
        default=now + dt.timedelta(days=1),
        help="End date for the suite. Accepted temporal formats are those "
             "supported by the python-dateutil package (YYYYMMDD and "
             "YYYY-MM-DD work, others might too)"
    )
    parser.add_argument(
        "-f", "--force-suite-overwrite",
        action="store_true",
        help="Should an already existing suite be overwritten with the new "
             "definition when sending to the ecflow server? Defaults to False, "
             "meaning that suites that have the same name as already existing "
             "suites will not be sent to the ecflow server. This parameter is "
             "only meaningful when the server credentials are also supplied."
    ),
    parser.add_argument(
        "-p", "--play-suites",
        action="store_true",
        help="Play suites after sending them to the ecflowserver. The default "
             "behaviour is to put new suites in a SUSPENDED state and wait "
             "for an operator to explicitly resume them. When using this flag "
             "the suites will be automatically started after loading"
    ),
    parser.add_argument(
        "--select-suite",
        action="append",
        help="Choose only certain suites from the configuration file to be "
             "loaded. This argument can be specified multiple times"
    ),
    parser.add_argument(
        "--select-hour",
        action="append",
        help="Choose only certain hours from the configuration file to be "
             "loaded. This argument can be specified multiple times"
    ),
    parser.add_argument(
        "--select-minute",
        action="append",
        help="Choose only certain minutes from the configuration file to be "
             "loaded. This argument can be specified multiple times"
    ),
    parser.add_argument(
        "--select-product",
        action="append",
        help="Choose only certain product families from the configuration "
             "file to be loaded. This argument can be specified multiple "
             "times"
    ),
    parser.add_argument(
        "--start-at-task",
        help="Load a subset of the suite defined in the configuration file "
             "by ignoring tasks that are upstream of this one"
    ),
    parser.add_argument(
        "-o", "--output-definition-file",
        help="Path to the ecflow definition file that is to be created. If "
             "not specified the ecflow definition will be printed to standard "
             "output."
    ),
    parser.add_argument(
        "--ecflow-host",
        help="If specified, the resulting ecflow definition will be loaded "
             "into the ecflow server"
    ),
    parser.add_argument(
        "--ecflow-port",
        default=3141,
        help="Port number where the ecflow server is running. This option is "
             "only used when specifying the --ecflow-host option."
    ),
    parser.add_argument(
        "--verbose",
        action="store_true",
        help="Display more information to standard output"
    )
    parser.add_argument(
        "--suites-home",
        default=os.path.expanduser("~/ECF_HOME"),
        help="Path where new suites will create their files. Each suite in "
             "the configuration file will set its own home "
             "as <suites-home>/<suite-name>"
    )
    parser.add_argument(
        "--dont-create-suite-files",
        action="store_true",
        help="Do not create the suites' ECF_HOME, ECF_FILES and ECF_OUT"
             "directories"
    )
    parser.add_argument(
        "--disregard-temporal-triggers",
        action="store_true",
        help="Do not create any temporal triggers. This might be useful for "
             "creating suites for backprocessing. This option causes each "
             "suite's name to be changed into "
             "<original_name>_no_temporal_triggers"
    )
    return parser


def _modify_hours(config, new_hours):
    for key, value in config.items():
        if key == "temporal_frequency":
            config[key]["hours"] = new_hours
        elif isinstance(value, dict):
            _modify_hours(value, new_hours)


def _modify_minutes(config, new_minutes):
    for key, value in config.items():
        if key == "temporal_frequency":
            value["minutes"] = new_minutes
        elif isinstance(value, dict):
            _modify_minutes(value, new_minutes)


def _modify_products(config, new_products):
    for key, value in config.items():
        if key == "products":
            config[key] = new_products
        elif isinstance(value, dict):
            _modify_products(value, new_products)


def _remove_temporal_triggers_config(config):
    for key, value in config.items():
        if key == "temporal_trigger_offset":
            del config[key]
        elif isinstance(value, dict):
            _remove_temporal_triggers_config(value)
    return config


def _rename_config_suites(config, name_pattern):
    conf = config.copy()
    for original_suite_name, suite_config in conf.items():
        new_name = name_pattern.format(suite=original_suite_name)
        if new_name != original_suite_name:
            conf[new_name] = suite_config
            conf.pop(original_suite_name)
    return conf


def _to_datetime(string):
    """Utility function to convert input arguments to a datetime object"""
    return dateutil.parser.parse(string)


if __name__ == "__main__":
    main()
